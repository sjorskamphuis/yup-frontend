import {Hero} from './hero';

export var HEROES: Hero[] = [
    {"id": 1, "name": "Catwoman"},
    {"id": 2, "name": "Black Widow"},
    {"id": 3, "name": "Hulk"},
    {"id": 4, "name": "Black Panter"},
    {"id": 5, "name": "Deadpool"},
    {"id": 6, "name": "Batman"},
    {"id": 7, "name": "Spiderman"},
    {"id": 8, "name": "Barney Stinson"},
    {"id": 9, "name": "Ironman"},
    {"id": 10, "name": "Captain Starbucks"}
];
