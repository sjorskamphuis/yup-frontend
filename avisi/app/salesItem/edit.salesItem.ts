/**
 * Created by sjors on 5/30/16.
 */
import {Component, OnInit}                  from 'angular2/core';
import {Injectable}                         from 'angular2/core';
import {Http, HTTP_PROVIDERS}               from "angular2/http";
import 'rxjs/add/operator/map';
import {Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteConfig, RouteParams} from
    'angular2/router';

@Component({
    selector: 'editSalesItem',
    templateUrl: 'templates/salesitem/edit.html',
    providers: [HTTP_PROVIDERS, ROUTER_PROVIDERS],
    directives: [ROUTER_DIRECTIVES]
})

@Injectable()
export class EditSalesItemComponent implements OnInit{

    private prefix = "/salesItems/";
    public salesItem = {
        taxId : null,
        tax: {
            id : null
        },
        typeId: null,
        type: {
            id: null
        }
    };
    public message = "";
    private id;
    public types = [];
    public taxes = [];

    constructor(private http:Http, private params: RouteParams) {
        this.id = params.get('id');
    }

     ngOnInit() {
        console.log('test');
        this.index();
        this.getTypes();
        this.getTaxes();
    }

    public index() {
        console.log('baas');
        this.http.get('http://localhost:8080' + this.prefix + this.id)
            .map(res => res.json())
            .subscribe(
                data => this.salesItem = data.salesItem,
                err => this.message = err,
                () => this.setDefaultValues()
            );
    }

    public setDefaultValues() {
        this.salesItem.taxId = this.salesItem.tax.id;
        this.salesItem.typeId = this.salesItem.type.id;
    }

    public getTypes() {
        this.http.get('http://localhost:8080/types/')
            .map(res => res.json())
            .subscribe(
                data => this.types = data.types,
                err => this.message = err
            );
    }

    public getTaxes() {
        this.http.get('http://localhost:8080/taxes/')
            .map(res => res.json())
            .subscribe(
                data => this.taxes = data.taxes,
                err => this.message = err
            );
    }

    public update() {
        this.http.put('http://localhost:8080' + this.prefix + this.id, JSON.stringify(this.salesItem))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err)
            );
    }
}
