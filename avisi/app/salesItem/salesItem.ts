/**
 * Created by sjors on 5/31/16.
 */
import {Component, OnInit, Injectable}                  from 'angular2/core';
import {Http, HTTP_PROVIDERS}                           from "angular2/http";
import 'rxjs/add/operator/map';

@Component({
    selector: 'salesItem',
    templateUrl: 'templates/salesItem.html',
    providers: [HTTP_PROVIDERS],
})

@Injectable()
export class SalesItemComponent implements OnInit {

    public salesItems = [];
    public salesItem = {
        name: null,
        description: null,
        price: null,
        sku: null,
        typeId: null,
        taxId: null
    };
    public types = [];
    public taxes = [];
    public item;
    public createForm = false;
    public message = "";

    constructor(private http:Http) {}

    ngOnInit() {
        this.index();
        this.getTypes();
        this.getTaxes();
    }

    public index() {
        this.http.get('http://localhost:8080/salesItems/')
            .map(res => res.json())
            .subscribe(
                data => this.salesItems = data.salesItems,
                err => this.message = err
            );
    }

    public getTypes() {
        this.http.get('http://localhost:8080/types/')
            .map(res => res.json())
            .subscribe(
                data => this.types = data.types,
                err => this.message = err
            );
    }

    public getTaxes() {
        this.http.get('http://localhost:8080/taxes/')
            .map(res => res.json())
            .subscribe(
                data => this.taxes = data.taxes,
                err => this.message = err
            );
    }

    public create() {
        this.message = "";
        this.createForm = !this.createForm;
    }

    public store() {
        this.http.post('http://localhost:8080/salesItems/', JSON.stringify(this.salesItem))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => this.message = err,
                () => this.index()
            );
        
        this.createForm = !this.createForm;
    }

    public drop(id) {
        this.http.delete("http://localhost:8080/salesItems/" + id)
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err),
                () => this.index()
            );
    }
}