import {Component, OnInit}                   from 'angular2/core';
import {Injectable}                    from 'angular2/core';
import {Http, HTTP_PROVIDERS}           from "angular2/http";
import 'rxjs/add/operator/map';

@Component({
    selector: 'contactPersons',
    templateUrl: 'templates/contactPersons.html',
    providers: [HTTP_PROVIDERS],
})

@Injectable()
export class contactPersonsComponent implements OnInit{

    private prefix = "/contactPersons/";
    public createForm = false;
    public contactPerson = {};
    public contactPersons = [];
    public companies;
    public message = "";

    constructor(private http:Http) {}

    ngOnInit() {
        this.index();
        this.getCompanies();
    }

    protected getCompanies() {
        this.http.get('http://localhost:8080/companies/')
            .map(res => res.json())
            .subscribe()
    }

    public index() {
        console.log('get all contact persons...');
        this.http.get('http://localhost:8080' + this.prefix)
            .map(res => res.json())
            .subscribe(
                data => this.contactPersons = data.types,
                err => this.message = err.message
            );
    }

    public create() {
        this.message = null;
        return this.createForm = !this.createForm;
    }

    public store() {
        this.http.post('http://localhost:8080' + this.prefix, JSON.stringify(this.contactPerson))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => this.message = err.message
            );

        this.createForm = !this.createForm;
        this.index();
    }

    public drop(id) {
        this.http.delete("http://localhost:8080" + this.prefix + id)
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err)
            );
    }
}
