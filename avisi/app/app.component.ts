import { Component }       									from 'angular2/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import { DashboardComponent }								from './dashboard.component';
import { TypeComponent }								    from './type/type';
import { EditTypeComponent }								from './type/edit.type';

import { CompanyComponent }									from './company/company';
import { EditCompanyComponent }								from './company/edit.company';

import { TaxComponent }										from './tax/tax';
import { EditTaxComponent }									from './tax/edit.tax';

import { SalesItemComponent }								from './salesItem/salesItem';
import { EditSalesItemComponent }						    from './salesItem/edit.salesItem';

import {OrderComponent} 									from "./order/order";
import {EditOrderComponent} 								from "./order/edit.order";

import {SenderComponent} 									from "./sender/sender";
import {EditSenderComponent} 								from "./sender/edit.sender";


@Component ({
	selector: 'app',
	templateUrl: '/templates/default.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [
        ROUTER_PROVIDERS
    ]
})

@RouteConfig([
	// {
	// 	path: '/product',
    	// name: 'Product',
    	// component: ProductComponent
	// },

	//types
	{
		path: '/types',
		name: 'Types',
		component: TypeComponent
	},
    {
        path: '/types/edit/:id',
        name: 'EditTypes',
        component: EditTypeComponent
    },

	//taxes
	{
		path: '/taxes',
		name: 'Taxes',
		component: TaxComponent
	},
	{
		path: '/taxes/edit/:id',
		name: 'EditTaxes',
		component: EditTaxComponent
	},

	//salesItem
	{
		path: '/salesItems',
		name: 'SalesItems',
		component: SalesItemComponent
	},
	{
		path: '/salesItem/edit/:id',
		name: 'EditSalesItem',
		component: EditSalesItemComponent
	},

	//orders
	{
		path: '/orders',
		name: 'Orders',
		component: OrderComponent
	},

	{
		path: '/orders/edit/:id',
		name: 'EditOrder',
		component: EditOrderComponent
	},

    //company
    {
		path: '/companies',
		name: 'Company',
		component: CompanyComponent
    },
	{
		path: '/companies/edit/:id',
		name: 'EditCompany',
		component: EditCompanyComponent
	},
    //
	// {
	// 	path: '/contactPersons',
	// 	name: 'ContactPersons',
	// 	component: contactPersonsComponent
	// },
	// {
	// 	path: '/contactPerson/edit.contactPersons/:id',
	// 	name: 'EditContactPersons',
	// 	component: EditContactPersonsComponent
	// },
	{
		path: '/senders',
		name: 'Sender',
		component: SenderComponent
	},
	{
		path: '/senders/edit/:id',
		name: 'EditSender',
		component: EditSenderComponent
	},

    {
        path: '/dashboard',
    	name: 'Dashboard',
    	component: DashboardComponent,
    	useAsDefault: true
    }
])

export class AppComponent {

}
