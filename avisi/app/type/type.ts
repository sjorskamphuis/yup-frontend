import {Component, OnInit}                   from 'angular2/core';
import {Injectable}                    from 'angular2/core';
import {Http, HTTP_PROVIDERS}           from "angular2/http";
import 'rxjs/add/operator/map';

@Component({
    selector: 'types',
    templateUrl: 'templates/types.html',
    providers: [HTTP_PROVIDERS],
})

@Injectable()
export class TypeComponent implements OnInit{

    private prefix = "/types/";
    public createForm = false;
    public type = {name: ""};
    public types = [];
    public message = "";

    constructor(private http:Http) {}

    ngOnInit() {
        this.index();
    }

    public index() {
        console.log('get all types...');
        this.http.get('http://localhost:8080' + this.prefix)
            .map(res => res.json())
            .subscribe(
                data => this.types = data.types,
                err => this.message = err.message
            );
    }

    public create() {
        this.message = "";
        return this.createForm = !this.createForm;
    }

    public store() {
        this.http.post('http://localhost:8080' + this.prefix, JSON.stringify(this.type))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => this.message = err.message,
                () => this.index()
        );

        this.createForm = !this.createForm;
        this.type.name = null;
        this.index();
    }

    public drop(id) {
        this.http.delete("http://localhost:8080" + this.prefix + id)
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err),
                () => this.index()
            );
    }
}
