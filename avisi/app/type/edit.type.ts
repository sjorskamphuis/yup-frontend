/**
 * Created by sjors on 5/30/16.
 */
import {Component, OnInit}                   from 'angular2/core';
import {Injectable}                    from 'angular2/core';
import {Http, HTTP_PROVIDERS}           from "angular2/http";
import 'rxjs/add/operator/map';
import {Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteConfig, RouteParams} from
    'angular2/router';

@Component({
    selector: 'editTypes',
    templateUrl: 'templates/type/edit.html',
    providers: [HTTP_PROVIDERS, ROUTER_PROVIDERS],
    directives: [ROUTER_DIRECTIVES]
})

@Injectable()
export class EditTypeComponent implements OnInit{

    private prefix = "/types/";
    public type = {};
    public message = "";
    private id;

    constructor(private http:Http, private params: RouteParams) {
        this.id = params.get('id');
    }

    ngOnInit() {
        this.index();
    }

    public index() {
        this.http.get('http://localhost:8080' + this.prefix + this.id)
            .map(res => res.json())
            .subscribe(
                data => this.type = data.type,
                err => this.message = err.message
            );
    }

    public update() {
        this.http.put('http://localhost:8080' + this.prefix + this.id, JSON.stringify(this.type))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err)
            );
    }
}
