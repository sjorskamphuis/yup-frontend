import {Component}                   from 'angular2/core';
import {Injectable}                    from 'angular2/core';
import {Http, HTTP_PROVIDERS}           from "angular2/http";
import 'rxjs/add/operator/map';

@Component({
    selector: 'customers',
    templateUrl: 'templates/customers.html',
    providers: [HTTP_PROVIDERS],
})

@Injectable()
export class CustomersComponent {

    constructor(private http:Http) {}

    // urls used for customers
    private prefix = '/customers/';
    private addUrl = 'add/';
    private editUrl = '/edit/';
    private deleteUrl = 'delete/';

    // get list of customers
    protected customers = this.allCustomers();

    protected defaultAdd = { dialog: false, customer: { contactPersons: {} } };
    protected add = {
        dialog: false,
        customer: {
            contactPersons: {}
        }
    };

    protected delete = {
        id: null,
        dialog: false,
    };

    protected edit = {
        dialog: false,
        customer: {
            id: null
        }
    };

    // all customers
    protected allCustomers() {
        this.http.get('http://localhost:8080' + this.prefix)
            .map(res => res.json())
            .subscribe(
                data => this.customers = data.customers,
                err => this.customers = err,
                () => console.log(this.customers)
            );
    }

    // add a customer
    public store() {
        this.http.post('http://localhost:8080' + this.prefix + this.addUrl, JSON.stringify(this.add.customer))
            .map(res => res.json())
            .subscribe(
                data => console.log(data),
                err => console.log(err),
                () => this.customers = this.allCustomers()
            );
        // disable modal
        this.cancel();
    }

    // update a customer
    public update() {
        this.http.put('http://localhost:8080' + this.prefix + this.edit.customer.id + this.editUrl, JSON.stringify(this.edit.customer))
            .map(res => res.json())
            .subscribe(
                data => console.log(data),
                err => console.log(err),
                () => this.customers = this.allCustomers() // reload list with customers
            );

        // disable modal
        this.cancel();
    }

    // delete a customer
    public drop() {
        this.http.delete('http://localhost:8080' + this.prefix + this.deleteUrl + this.delete.id)
            .map(res => res.json())
            .subscribe(
                data => console.log(data),
                err => console.log(err),
                () => this.customers = this.allCustomers() // reload list with products
            );

        // disable modal
        this.cancel();
    }

    // cancel the delete request (No button)
    public cancel() {
        //add
        this.add.dialog = false;
        this.add = this.defaultAdd;

        //edit
        this.edit.customer = null;
        this.edit.dialog = false;

        //delete
        this.delete.id = null;
        this.delete.dialog = false;
    }

    // show add form
    public showAddForm() {
        this.add.dialog = !this.add.dialog;
    }
    // show edit form
    public showUpdateForm(product) {
        this.edit.customer = product;
        this.edit.dialog = !this.edit.dialog;
    }

    // show delete dialog
    public showDeleteDialog(id) {
        this.delete.id = id;
        return this.delete.dialog = !this.delete.dialog;
    }
}
