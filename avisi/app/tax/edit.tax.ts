/**
 * Created by sjors on 5/30/16.
 */
import {Component, OnInit}                   from 'angular2/core';
import {Injectable}                    from 'angular2/core';
import {Http, HTTP_PROVIDERS}           from "angular2/http";
import 'rxjs/add/operator/map';
import {Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteConfig, RouteParams} from
    'angular2/router';

@Component({
    selector: 'editTaxes',
    templateUrl: 'templates/tax/edit.html',
    providers: [HTTP_PROVIDERS, ROUTER_PROVIDERS],
    directives: [ROUTER_DIRECTIVES]
})

@Injectable()
export class EditTaxComponent implements OnInit{

    private prefix = "/taxes/";
    public tax = {};
    public message = "";
    private id;

    constructor(private http:Http, private params: RouteParams) {
        this.id = params.get('id');
    }

    ngOnInit() {
        this.index();
    }

    public index() {
        this.http.get('http://localhost:8080' + this.prefix + this.id)
            .map(res => res.json())
            .subscribe(
                data => this.tax = data.tax,
                err => this.message = err
            );


    }

    public update() {
        this.http.put('http://localhost:8080' + this.prefix + this.id, JSON.stringify(this.tax))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err)
            );
    }
}
