import {Component, OnInit}                   from 'angular2/core';
import {Injectable}                    from 'angular2/core';
import {Http, HTTP_PROVIDERS}           from "angular2/http";
import 'rxjs/add/operator/map';

@Component({
    selector: 'taxes',
    templateUrl: 'templates/taxes.html',
    providers: [HTTP_PROVIDERS],
})

@Injectable()
export class TaxComponent implements OnInit{

    private prefix = "/taxes/";
    public createForm = false;
    public tax = {name: ""};
    public taxes = [];
    public message = "";

    constructor(private http:Http) {}

    ngOnInit() {
        this.index();
    }

    public index() {
        console.log('get all taxes...');
        this.http.get('http://localhost:8080' + this.prefix)
            .map(res => res.json())
            .subscribe(
                data => this.taxes= data.taxes,
                err => this.message = err.message
            );
    }

    public create() {
        this.message = "";
        return this.createForm = !this.createForm;
    }

    public store() {
        this.http.post('http://localhost:8080' + this.prefix, JSON.stringify(this.tax))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => this.message = err.message,
                () => this.index()
        );

        this.createForm = !this.createForm;
        this.tax.name = null
    }

    public drop(id) {
        this.http.delete("http://localhost:8080" + this.prefix + id)
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err),
                () => this.index()
            );
    }
}
