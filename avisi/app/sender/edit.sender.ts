import {Component, OnInit}                   from 'angular2/core';
import {Injectable}                    from 'angular2/core';
import {Http, HTTP_PROVIDERS}           from "angular2/http";
import 'rxjs/add/operator/map';
import {Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteConfig, RouteParams} from
    'angular2/router';


@Component({
    selector: 'editSender',
    templateUrl: 'templates/sender/edit.html',
    providers: [HTTP_PROVIDERS, ROUTER_PROVIDERS],
    directives: [ROUTER_DIRECTIVES]
})

@Injectable()
export class EditSenderComponent implements OnInit{

    private prefix = "/senders/";
    public sender = {};
    public message = "";
    private id;

    constructor(private http:Http, private params: RouteParams) {
        this.id = params.get('id');
    }

    ngOnInit() {
        this.index();
    }

    public index() {
        this.http.get('http://localhost:8080' + this.prefix + this.id)
            .map(res =>  res.json())
            .subscribe(
                data => this.sender = data.sender,
                err => this.message = err.message,
                () => console.log(this.sender)
            );
        console.log('http://localhost:8080' + this.prefix + this.id);
    }

    public update() {
        this.http.put('http://localhost:8080' + this.prefix + this.id, JSON.stringify(this.sender))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err)
            );
    }
}
