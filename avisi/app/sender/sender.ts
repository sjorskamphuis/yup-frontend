import {Component, OnInit}                   from 'angular2/core';
import {Injectable}                    from 'angular2/core';
import {Http, HTTP_PROVIDERS}           from "angular2/http";
import 'rxjs/add/operator/map';

@Component({
    selector: 'senders',
    templateUrl: 'templates/sender.html',
    providers: [HTTP_PROVIDERS],
})

@Injectable()
export class SenderComponent implements OnInit{

    private prefix = "/senders/";
    public createForm = false;
    public sender = {name: "", address: "", zipcode:"", city:"", kvkNumber:"", btwNumber:"", bicnumber:"", template:"", website:""};
    public senders = [];
    public message = "";

    constructor(private http:Http) {}

    ngOnInit() {
        this.index();
    }

    public index() {
        console.log('get all senders...');
        this.http.get('http://localhost:8080' + this.prefix)
            .map(res => res.json())
            .subscribe(
                data => this.senders = data.senders,
                err => this.message = err.message
            );
    }

    public create() {
        this.message = "";
        return this.createForm = !this.createForm;
    }

    public store() {
        this.http.post('http://localhost:8080' + this.prefix, JSON.stringify(this.sender))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => this.message = err.message,
                () => this.index()
            );

        this.createForm = !this.createForm;
        this.sender.name = null;
    }

    public drop(id) {
        this.http.delete("http://localhost:8080" +this.prefix + id)
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err),
                () => this.index()
            );
    }
}
