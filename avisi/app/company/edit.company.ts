import {Component, OnInit}                   from 'angular2/core';
import {Injectable}                    from 'angular2/core';
import {Http, HTTP_PROVIDERS}           from "angular2/http";
import 'rxjs/add/operator/map';
import {Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteConfig, RouteParams} from
    'angular2/router';

@Component({
    selector: 'editCompany',
    templateUrl: 'templates/company/edit.html',
    providers: [HTTP_PROVIDERS, ROUTER_PROVIDERS],
    directives: [ROUTER_DIRECTIVES]
})

@Injectable()
export class EditCompanyComponent implements OnInit{

    private prefix = "/companies/";
    public company = {email:"", invoiceEmail: "", contactPersons:[]};
    //public contactPerson = {};
    public message = '';
    private id;
    private contactPersonProperties = {
        firstName: "",
        lastName: "",
        email: "",
        telephone: ""
    };
    protected contactPerson = JSON.parse(JSON.stringify(this.contactPersonProperties));

    constructor(private http:Http, private params: RouteParams) {
        this.id = params.get('id');
    }

    ngOnInit() {
        this.index();
    }

    public index() {
        this.http.get('http://localhost:8080' + this.prefix + this.id)
            .map(res => res.json())
            .subscribe(
                data => this.company = data.company,
                err => this.message = err.message,
                () => console.log(this.company)
            );
    }

    public update() {

        this.company.email = this.company.invoiceEmail;

        this.http.put('http://localhost:8080' + this.prefix + this.id, JSON.stringify(this.company))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err)
            );
    }

    public addContact() {
        if (
            this.contactPerson.firstName != "" &&
            this.contactPerson.lastName != "" &&
            this.contactPerson.email != "" &&
            this.contactPerson.telephone != ""
        ) {
            this.company.contactPersons.push(this.contactPerson);
            this.contactPerson = JSON.parse(JSON.stringify(this.contactPersonProperties));
            return true;
        } else {
            return false;
        }

    }
}
