/**
 * Created by sjors on 6/1/16.
 */

import {Component, OnInit, Injectable}                   from 'angular2/core';
import {Http, HTTP_PROVIDERS}                                   from "angular2/http";
import 'rxjs/add/operator/map';

@Component({
    selector: 'company',
    templateUrl: 'templates/companies.html',
    providers: [HTTP_PROVIDERS],
})

export class CompanyComponent implements OnInit {

    public createForm = false;

    protected companyProperties = {
        contactPersons: []
    };
    protected companies = [];
    protected company = JSON.parse(JSON.stringify(this.companyProperties));
    private contactPersonProperties = {
        firstName: "",
        lastName: "",
        email: "",
        telephone: ""
    };
    protected message = "";
    protected contactPerson = JSON.parse(JSON.stringify(this.contactPersonProperties));

    public constructor(private http:Http) {}

    ngOnInit() {
        this.index();
    }

    public index() {
        this.http.get("http://localhost:8080/companies/")
            .map(res => res.json())
            .subscribe(
                data => this.companies = data.companies,
                err => this.message = err
            )
    }


    public create() {
        this.createForm = !this.createForm;
    }

    public store() {
        console.log(this.company.contactPersons.length);
        if(this.company.contactPersons.length < 0) {
            if(!this.addContact()) {
                // todo: throw error, contactPerson need to be filled in
                this.message = "Some fields needs to be filled in";
                console.log('please throw an error')
            } else {
                this.addContact();
                this.postData();
            }
        } else {
            // just store
            console.log('store the items');
            this.addContact();
            this.postData();
        }
    }

    public postData() {
        this.http.post('http://localhost:8080/companies/', JSON.stringify(this.company))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => this.message = err,
                () => this.index()
            );

        this.createForm = !this.createForm;
        this.company = JSON.parse(JSON.stringify(this.companyProperties));
    }

    public addContact() {
        if (
            this.contactPerson.firstName != "" &&
            this.contactPerson.lastName != "" &&
            this.contactPerson.email != "" &&
            this.contactPerson.telephone != ""
        ) {
            this.company.contactPersons.push(this.contactPerson);
            this.contactPerson = JSON.parse(JSON.stringify(this.contactPersonProperties));
            return true;
        } else {
            return false;
        }

    }

    public drop(id) {
        this.http.delete("http://localhost:8080/companies/" + id)
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err),
                () => this.index()
            );
    }
}
