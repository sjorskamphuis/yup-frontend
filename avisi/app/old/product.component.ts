import {Component}                   from 'angular2/core';
import {Injectable}                    from 'angular2/core';
import {Http, HTTP_PROVIDERS}           from "angular2/http";
import 'rxjs/add/operator/map';

@Component({
    selector: 'product',
    templateUrl: 'templates/products/overview.html',
    providers: [HTTP_PROVIDERS],
})

@Injectable()
export class ProductComponent {

    constructor(private http:Http) {}

    // urls used for products
    private prefix = '/products/';
    private addUrl = 'add/';
    private editUrl = '/edit/';
    private deleteUrl = 'delete/';

    // get list of products
    protected products = this.allProducts();

    protected  add = {
        dialog: false,
        product: { typeId: null, sku: "" }
    };

    protected delete = {
        id: null,
        dialog: false,
    };

    protected edit = {
        dialog: false,
        product: {
            id: null,
            typeId: null
        }
    };

    // all products
    protected allProducts() {
        this.http.get('http://localhost:8080' + this.prefix)
            .map(res => res.json())
            .subscribe(
                data => this.products = data.products,
                err => this.products = err,
                () => console.log(this.products)
            );
    }

    // add a product
    public store() {
        this.add.product.typeId = "573adf2134c7c9fac91bf066"; // todo: make this dynamic
        this.http.post('http://localhost:8080' + this.prefix + this.addUrl, JSON.stringify(this.add.product))
            .map(res => res.json())
            .subscribe(
                data => console.log(data),
                err => console.log(err),
                () => this.products = this.allProducts()
            );
        // disable modal
        this.cancel();
    }

    // update a product
    public update() {
        this.edit.product.typeId = "573adf2134c7c9fac91bf066"; // todo: make this dynamic
        this.http.put('http://localhost:8080' + this.prefix + this.edit.product.id + this.editUrl, JSON.stringify(this.edit.product))
            .map(res => res.json())
            .subscribe(
                data => console.log(data),
                err => console.log(err),
                () => this.products = this.allProducts() // reload list with products
            );

        // disable modal
        this.cancel();
    }

    // delete a product
    public drop() {
        this.http.delete('http://localhost:8080' + this.prefix + this.deleteUrl + this.delete.id)
            .map(res => res.json())
            .subscribe(
                data => console.log(data),
                err => console.log(err),
                () => this.products = this.allProducts() // reload list with products
            );

        // disable modal
        this.cancel();
    }

    // cancel the delete request (No button)
    public cancel() {
        //add
        this.add.dialog = false;
        this.add.product = null;

        //edit
        this.edit.product = null;
        this.edit.dialog = false;

        //delete
        this.delete.id = null;
        this.delete.dialog = false;
    }

    // show add form
    public showAddForm() {
        this.add.dialog = !this.add.dialog;
    }
    // show edit form
    public showUpdateForm(product) {
        this.edit.product = product;
        this.edit.dialog = !this.edit.dialog;
    }

    // show delete dialog
    public showDeleteDialog(id) {
        this.delete.id = id;
        return this.delete.dialog = !this.delete.dialog;
    }
}
