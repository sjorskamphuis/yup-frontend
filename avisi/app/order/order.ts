/**
 * Created by sjors on 5/31/16.
 */
import {Component, OnInit}                   from 'angular2/core';
import {Injectable}                    from 'angular2/core';
import {Http, HTTP_PROVIDERS}           from "angular2/http";

import 'rxjs/add/operator/map';

@Component({
    selector: 'orders',
    templateUrl: 'templates/orders.html',
    providers: [HTTP_PROVIDERS],
})

@Injectable()
export class OrderComponent implements OnInit{

    public createForm = false;
    public orders = [];
    public order = {
        number: null,
        period: "Monthly",
        companyId : "",
        orderLines: [],
        message: ""
    };
    protected message = "";
    protected contactPersons = [];
    protected companies = [];
    protected senders = [];
    protected salesItems = [];
    protected orderLineProperties = {
        salesItemId: "",
        salesItem: {},
        quantity: ""
    };
    protected orderLine = JSON.parse(JSON.stringify(this.orderLineProperties));

    constructor(private http:Http) {}
    ngOnInit() {
        this.index();
        this.getCompanies();
        this.getSenders();
        this.getSalesItems();
    }

    public index() {
        this.http.get("http://localhost:8080/orders/")
            .map(res => res.json())
            .subscribe(
                data => this.orders = data.orders,
                err => console.log("error, orders..."),
                () => console.log(this.orders)
            );
    }

    public create() {
        this.createForm = !this.createForm;
    }

    public getCompanies() {
        this.http.get("http://localhost:8080/companies/")
            .map(res => res.json())
            .subscribe(
                data => this.companies = data.companies,
                err => this.message = err,
                () => this.reloadContactPersons(this.companies[0].id)
            );
    }

    public getSenders() {
        this.http.get("http://localhost:8080/senders/")
            .map(res => res.json())
            .subscribe(
                data => this.senders = data.senders,
                err => this.message = err
            );
    }

    public getSalesItems() {
        this.http.get("http://localhost:8080/salesItems/")
            .map(res => res.json())
            .subscribe(
                data => this.salesItems = data.salesItems,
                err => this.message = err
            )
    }

    public addOrderLine() {

        for(var i=0; i < this.salesItems.length; i++) {
            if(this.orderLine.salesItemId === this.salesItems[i].id) {
                this.orderLine.salesItem = this.salesItems[i];
            }
        }
        this.order.orderLines.push(this.orderLine);
        this.orderLine = JSON.parse(JSON.stringify(this.orderLineProperties));
    }

    public store() {

        console.log(this.order);

        this.http.post("http://localhost:8080/orders/", JSON.stringify(this.order))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => this.message = err,
                () => this.index()
            );

        this.createForm = !this.createForm;
    }

    public reloadContactPersons(val) {
        if(val.target != undefined) {
            this.order.companyId = val.target.value;
        } else {
            this.order.companyId = val;
        }
        this.http.get("http://localhost:8080/companies/" + this.order.companyId)
            .map(res => res.json())
            .subscribe(
                data => this.contactPersons = data.company.contactPersons,
                err => this.message = err
            );
    }

    public drop(id) {
        this.http.delete("http://localhost:8080/orders/" + id)
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err),
                () => this.index()
            );
    }
}
