import {Component, OnInit}                   from 'angular2/core';
import {Injectable}                    from 'angular2/core';
import {Http, HTTP_PROVIDERS}           from "angular2/http";
import 'rxjs/add/operator/map';
import {Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteConfig, RouteParams} from
    'angular2/router';


@Component({
    selector: 'EditOrder',
    templateUrl: 'templates/orders/edit.html',
    providers: [HTTP_PROVIDERS, ROUTER_PROVIDERS],
    directives: [ROUTER_DIRECTIVES]
})

@Injectable()
export class EditOrderComponent implements OnInit{
    private prefix = "/orders/";
    public order = {};
    public message = null;
    private id;
    private jsonS;

    constructor(private http:Http, private params: RouteParams) {
        this.id = params.get('id');
    }

    ngOnInit() {
        this.index();
    }

    public index() {
        this.http.get('http://localhost:8080' + this.prefix + this.id)
            .map(res =>  res.json())
            .subscribe(
                data => this.order = data.order,
                err => this.message = err.message
            );
        console.log('http://localhost:8080' + this.prefix + this.id);
    }

    public update() {
        this.http.put('http://localhost:8080' + this.prefix + this.id, JSON.stringify(this.order))
            .map(res => res.json())
            .subscribe(
                data => this.message = data.message,
                err => console.log(err)
            );
        console.log('http://localhost:8080' + this.prefix + this.id);
    }
}
